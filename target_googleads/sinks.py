"""GoogleAds target sink class, which handles writing streams."""


from singer_sdk.sinks import RecordSink
from target_googleads.client import GoogleClient


class GoogleAdsSink(RecordSink):
    """GoogleAds target sink class."""

    client = None

    def get_client(self):
        if self.client is None:
            credentials = {
                "developer_token": self.config["developer_token"],
                "refresh_token": str(self.config["refresh_token"]),
                "client_id": str(self.config["client_id"]),
                "client_secret": str(self.config["client_secret"]),
                "use_proto_plus": self.config.get("use_proto_plus", True),
                "login_customer_id": self.config.get(
                    "manager_id", "143-409-4595"
                ).replace("-", ""),
            }

            self.client = GoogleClient(
                credentials, self.config.get("customer_id").replace("-", "")
            )

        return self.client

    def process_campaign(self, record):
        client = self.get_client()
        if record.get("adType", "app_ad") == "app_ad":
            client.create_app_ad_campaign(record)
        else:
            client.create_campaign(record)

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""

        if self.stream_name == "Campaigns":
            self.process_campaign(record)
        pass
