"""GoogleAds target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_googleads.sinks import (
    GoogleAdsSink,
)


class TargetGoogleAds(Target):
    """Sample target for GoogleAds."""

    name = "target-googleads"
    config_jsonschema = th.PropertiesList(
        th.Property("client_id", th.StringType, required=True),
        th.Property("client_secret", th.StringType, required=True),
        th.Property("developer_token", th.StringType, required=True),
        th.Property("customer_id", th.StringType, required=True),
        th.Property("manager_id", th.StringType, required=True),
    ).to_dict()
    default_sink_class = GoogleAdsSink


if __name__ == "__main__":
    TargetGoogleAds.cli()
