import argparse
import datetime
import sys
import uuid
import requests
import os
from dateutil.parser import parse

from google.ads.googleads.client import GoogleAdsClient
from google.ads.googleads.errors import GoogleAdsException


_DATE_FORMAT = "%Y%m%d"


class GoogleClient:
    def __init__(self, credentials, customer_id):
        self.client = self.client = GoogleAdsClient.load_from_dict(credentials)
        self.customer_id = customer_id

    def create_campaign(self, payload):
        client = self.client
        customer_id = self.customer_id
        # self.create_responsive_ad_object(payload)
        # customer_id = client.login_customer_id
        campaign_budget_service = client.get_service("CampaignBudgetService")
        campaign_service = client.get_service("CampaignService")
        # get_campaign(client,customer_id)

        # Create a budget, which can be shared by multiple campaigns.
        campaign_budget_operation = client.get_type("CampaignBudgetOperation")
        campaign_budget = campaign_budget_operation.create
        campaign_budget.name = f"Hotglue Budget {uuid.uuid4()}"
        campaign_budget.delivery_method = client.enums.BudgetDeliveryMethodEnum.STANDARD
        campaign_budget.amount_micros = int(payload.get("budget")) * 1000000

        # Add budget.
        try:
            campaign_budget_response = campaign_budget_service.mutate_campaign_budgets(
                customer_id=customer_id, operations=[campaign_budget_operation]
            )
        except GoogleAdsException as ex:
            handle_googleads_exception(ex)

        # Create campaign.
        campaign_operation = client.get_type("CampaignOperation")
        campaign = campaign_operation.create
        campaign.name = f"{payload.get('campaignName')} {uuid.uuid4()}"
        campaign.advertising_channel_type = (
            client.enums.AdvertisingChannelTypeEnum.SEARCH
        )

        # Recommendation: Set the campaign to PAUSED when creating it to prevent
        # the ads from immediately serving. Set to ENABLED once you've added
        # targeting and the ads are ready to serve.
        campaign.status = client.enums.CampaignStatusEnum.PAUSED

        # Set the bidding strategy and budget.
        campaign.manual_cpc.enhanced_cpc_enabled = True
        campaign.campaign_budget = campaign_budget_response.results[0].resource_name

        # Set the campaign network options.
        campaign.network_settings.target_google_search = True
        campaign.network_settings.target_search_network = True
        campaign.network_settings.target_partner_search_network = False
        # Enable Display Expansion on Search campaigns. For more details see:
        # https://support.google.com/google-ads/answer/7193800
        campaign.network_settings.target_content_network = True

        # Optional: Set the start date.
        if payload.get("startDate"):
            start_time = parse(payload["startDate"])
        else:
            start_time = datetime.date.today() + datetime.timedelta(days=1)
        campaign.start_date = datetime.date.strftime(start_time, _DATE_FORMAT)

        # Optional: Set the end date.
        if payload.get("endDate"):
            end_time = parse(payload["endDate"])
        else:
            end_time = start_time + datetime.timedelta(weeks=4)
        campaign.end_date = datetime.date.strftime(end_time, _DATE_FORMAT)

        # Add the campaign.
        try:
            campaign_response = campaign_service.mutate_campaigns(
                customer_id=customer_id, operations=[campaign_operation]
            )
            print(f"Created campaign {campaign_response.results[0].resource_name}.")
            if payload.get("adGroup"):
                # create and get ad Object
                if payload.get("adType", "responsive") == "responsive":
                    self.create_responsive_ad_object(payload)
                campaign_id = self.extract_id(
                    campaign_response.results[0].resource_name
                )
                ad_group = self.create_ad_group(campaign_id, payload)
                ad_group_id = self.extract_id(ad_group)
                if payload.get("keywords"):
                    if len(payload["keywords"]) > 0:
                        for keyword in payload["keywords"]:
                            keyword_res = self.add_keyword(ad_group_id, keyword)
        except GoogleAdsException as ex:
            handle_googleads_exception(ex)

    def create_responsive_ad_object(self, payload):
        payload
        pass

    def extract_id(self, result):
        retur_res = ""
        res = result.split("/")
        if len(res) > 0:
            try:
                retur_res = res[len(res) - 1]
                retur_res = int(retur_res)
            except:
                retur_res = ""
        return retur_res

    def micros_to_currency(micros):
        return micros / 1000000.0 if micros is not None else None

    def get_budgets(self, client, customer_id):
        ga_service = client.get_service("GoogleAdsService")

        query = """
            SELECT
            account_budget.status,
            account_budget.billing_setup,
            account_budget.approved_spending_limit_micros,
            account_budget.approved_spending_limit_type,
            account_budget.proposed_spending_limit_micros,
            account_budget.proposed_spending_limit_type,
            account_budget.adjusted_spending_limit_micros,
            account_budget.adjusted_spending_limit_type,
            account_budget.approved_start_date_time,
            account_budget.proposed_start_date_time,
            account_budget.approved_end_date_time,
            account_budget.approved_end_time_type,
            account_budget.proposed_end_date_time,
            account_budget.proposed_end_time_type
            FROM account_budget"""

        stream = ga_service.search_stream(customer_id=customer_id, query=query)

        for batch in stream:
            for row in batch.results:
                budget = row.account_budget

                # Here and in the statements below, the variable is set to the
                # name of the Enum as a default if the numeric value for the
                # monetary or date fields is not present.
                approved_spending_limit = (
                    self.micros_to_currency(budget.approved_spending_limit_micros)
                    or budget.approved_spending_limit_type.name
                )

                proposed_spending_limit = (
                    self.micros_to_currency(budget.proposed_spending_limit_micros)
                    or budget.proposed_spending_limit_type.name
                )

                adjusted_spending_limit = (
                    self.micros_to_currency(budget.adjusted_spending_limit_micros)
                    or budget.adjusted_spending_limit_type.name
                )

                approved_end_date_time = (
                    budget.approved_end_date_time or budget.approved_end_time_type.name
                )

                proposed_end_date_time = (
                    budget.proposed_end_date_time or budget.proposed_end_time_type.name
                )

                amount_served = (
                    self.micros_to_currency(budget.amount_served_micros) or 0.0
                )

                total_adjustments = (
                    self.micros_to_currency(budget.total_adjustments_micros) or 0.0
                )

                print(
                    f'Account budget "{budget.resource_name}", '
                    f'with status "{budget.status.name}", '
                    f'billing setup "{budget.billing_setup}", '
                    f"amount served {amount_served:.2f}, "
                    f"total adjustments {total_adjustments:.2f}, "
                    f'approved spending limit "{approved_spending_limit}" '
                    f'(proposed "{proposed_spending_limit}" -- '
                    f'adjusted "{adjusted_spending_limit}"), approved '
                    f'start time "{budget.approved_start_date_time}" '
                    f'(proposed "{budget.proposed_start_date_time}"), '
                    f'approved end time "{approved_end_date_time}" '
                    f'(proposed "{proposed_end_date_time}").'
                )

    def create_ad_group(self, campaign_id, payload):
        client = self.client
        customer_id = self.customer_id
        ad_group_service = client.get_service("AdGroupService")
        campaign_service = client.get_service("CampaignService")

        # Create ad group.
        ad_group_operation = client.get_type("AdGroupOperation")
        ad_group = ad_group_operation.create
        ad_group.name = payload["adGroup"]
        ad_group.status = client.enums.AdGroupStatusEnum.ENABLED
        ad_group.campaign = campaign_service.campaign_path(customer_id, campaign_id)
        ad_group.type_ = client.enums.AdGroupTypeEnum.RESPONSIVE_SEARCH_AD
        # TODO we already defined budget, figure it out and bind it
        ad_group.cpc_bid_micros = 10000000
        if "images" in payload:
            image_assets = []
            for image in payload["images"]:
                image_assets.append(
                    self.upload_image_asset(image["url"], image["name"])
                )

            # ad_group.ad.app_ad.images.extend(image_assets)
            ad_group.ad.responsive_search_ad.images = image_assets
        # Add the ad group.
        ad_group_response = ad_group_service.mutate_ad_groups(
            customer_id=customer_id, operations=[ad_group_operation]
        )
        print(f"Created ad group {ad_group_response.results[0].resource_name}.")
        return ad_group_response.results[0].resource_name

    def add_keyword(self, ad_group_id, keyword_text):
        client = self.client
        customer_id = self.customer_id
        ad_group_service = client.get_service("AdGroupService")
        ad_group_criterion_service = client.get_service("AdGroupCriterionService")

        # Create keyword.
        ad_group_criterion_operation = client.get_type("AdGroupCriterionOperation")
        ad_group_criterion = ad_group_criterion_operation.create
        ad_group_criterion.ad_group = ad_group_service.ad_group_path(
            customer_id, ad_group_id
        )
        ad_group_criterion.status = client.enums.AdGroupCriterionStatusEnum.ENABLED
        ad_group_criterion.keyword.text = keyword_text
        ad_group_criterion.keyword.match_type = client.enums.KeywordMatchTypeEnum.EXACT

        # Optional field
        # All fields can be referenced from the protos directly.
        # The protos are located in subdirectories under:
        # https://github.com/googleapis/googleapis/tree/master/google/ads/googleads
        # ad_group_criterion.negative = True

        # Optional repeated field
        # ad_group_criterion.final_urls.append('https://www.example.com')

        # Add keyword
        ad_group_criterion_response = (
            ad_group_criterion_service.mutate_ad_group_criteria(
                customer_id=customer_id,
                operations=[ad_group_criterion_operation],
            )
        )

        print(
            "Created keyword "
            f"{ad_group_criterion_response.results[0].resource_name}."
        )
        return ad_group_criterion_response.results[0].resource_name

    def get_campaign(client, customer_id):
        ga_service = client.get_service("GoogleAdsService")

        query = """
            SELECT
            campaign.id,
            campaign.name
            FROM campaign
            ORDER BY campaign.id"""

        # Issues a search request using streaming.
        stream = ga_service.search_stream(customer_id=customer_id, query=query)

        for batch in stream:
            for row in batch.results:
                print(
                    f"Campaign with ID {row.campaign.id} and name "
                    f'"{row.campaign.name}" was found.'
                )

    # [START upload_image_asset]
    def upload_image_asset(self, url, asset_name):
        """Main method, to run this code example as a standalone application."""
        asset_resource = {}
        # Download image from URL
        image_content = requests.get(url).content
        client = self.client
        customer_id = self.customer_id
        asset_service = client.get_service("AssetService")
        asset_operation = client.get_type("AssetOperation")
        asset = asset_operation.create
        asset.type_ = client.enums.AssetTypeEnum.IMAGE
        asset.image_asset.data = image_content
        asset.image_asset.file_size = len(image_content)
        asset.image_asset.mime_type = client.enums.MimeTypeEnum.IMAGE_JPEG
        # Use your favorite image library to determine dimensions
        asset.image_asset.full_size.height_pixels = 315
        asset.image_asset.full_size.width_pixels = 600
        asset.image_asset.full_size.url = url
        # Provide a unique friendly name to identify your asset.
        # When there is an existing image asset with the same content but a different
        # name, the new name will be dropped silently.
        asset.name = asset_name

        mutate_asset_response = asset_service.mutate_assets(
            customer_id=customer_id, operations=[asset_operation]
        )
        print("Uploaded file(s):")
        for row in mutate_asset_response.results:
            resource_name = row.resource_name
            print(f"\tResource name: {row.resource_name}")
            asset_resource.update(
                {
                    "asset": row.resource_name,
                    # "file_size": asset.image_asset.file_size,
                    # "full_size": {"height_pixels": asset.image_asset.full_size.height_pixels, "width_pixels": asset.image_asset.full_size.width_pixels},
                    "mime_type": "image/jpeg",
                }
            )
        # [END upload_image_asset]

        return asset_resource

    def create_budget(self, client, customer_id):
        """Creates a budget under the given customer ID.

        Args:
            client: an initialized GoogleAdsClient instance.
            customer_id: a client customer ID str.

        Returns:
            A resource_name str for the newly created Budget.
        """
        # Retrieves the campaign budget service.
        campaign_budget_service = client.get_service("CampaignBudgetService")
        # Retrieves a new campaign budget operation object.
        campaign_budget_operation = client.get_type("CampaignBudgetOperation")
        # Creates a campaign budget.
        campaign_budget = campaign_budget_operation.create
        campaign_budget.name = f"Hotglue #{uuid.uuid4()}"
        campaign_budget.amount_micros = 50000000
        campaign_budget.delivery_method = client.enums.BudgetDeliveryMethodEnum.STANDARD
        # An App campaign cannot use a shared campaign budget.
        # explicitly_shared must be set to false.
        campaign_budget.explicitly_shared = False

        # Submits the campaign budget operation to add the campaign budget.
        response = campaign_budget_service.mutate_campaign_budgets(
            customer_id=customer_id, operations=[campaign_budget_operation]
        )
        resource_name = response.results[0].resource_name
        print(f'Created campaign budget with resource_name: "{resource_name}"')
        return resource_name

    def create_app_ad_campaign_resource(
        self, client, customer_id, budget_resource_name, payload
    ):
        """Creates an app campaign under the given customer ID.

        Args:
            client: an initialized GoogleAdsClient instance.
            customer_id: a client customer ID str.
            budget_resource_name: the budget to associate with the campaign

        Returns:
            A resource_name str for the newly created app campaign.
        """
        campaign_service = client.get_service("CampaignService")
        campaign_operation = client.get_type("CampaignOperation")
        campaign = campaign_operation.create
        campaign.name = f"Hotglue App #{uuid.uuid4()}"
        campaign.campaign_budget = budget_resource_name
        # Recommendation: Set the campaign to PAUSED when creating it to
        # prevent the ads from immediately serving. Set to ENABLED once you've
        # added targeting and the ads are ready to serve.
        campaign.status = client.enums.CampaignStatusEnum.PAUSED
        # All App campaigns have an advertising_channel_type of
        # MULTI_CHANNEL to reflect the fact that ads from these campaigns are
        # eligible to appear on multiple channels.
        campaign.advertising_channel_type = (
            client.enums.AdvertisingChannelTypeEnum.MULTI_CHANNEL
        )
        campaign.advertising_channel_sub_type = (
            client.enums.AdvertisingChannelSubTypeEnum.APP_CAMPAIGN
        )
        # Sets the target CPA to $1 / app install.
        #
        # campaign_bidding_strategy is a 'oneof' message so setting target_cpa
        # is mutually exclusive with other bidding strategies such as
        # manual_cpc, commission, maximize_conversions, etc.
        # See https://developers.google.com/google-ads/api/reference/rpc
        # under current version / resources / Campaign
        campaign.target_cpa.target_cpa_micros = 1000000
        # Sets the App Campaign Settings.
        campaign.app_campaign_setting.app_id = "com.google.android.apps.adwords"
        campaign.app_campaign_setting.app_store = (
            client.enums.AppCampaignAppStoreEnum.GOOGLE_APP_STORE
        )
        # Optimize this campaign for getting new users for your app.
        campaign.app_campaign_setting.bidding_strategy_goal_type = (
            client.enums.AppCampaignBiddingStrategyGoalTypeEnum.OPTIMIZE_INSTALLS_TARGET_INSTALL_COST
        )

        # Optional: Set the start date.
        if payload.get("startDate"):
            start_time = parse(payload["startDate"])
        else:
            start_time = datetime.date.today() + datetime.timedelta(days=1)
        campaign.start_date = datetime.date.strftime(start_time, _DATE_FORMAT)

        # Optional: Set the end date.
        if payload.get("endDate"):
            end_time = parse(payload["endDate"])
        else:
            end_time = start_time + datetime.timedelta(weeks=4)
        campaign.end_date = datetime.date.strftime(end_time, _DATE_FORMAT)
        # Optional: If you select the
        # OPTIMIZE_IN_APP_CONVERSIONS_TARGET_INSTALL_COST goal type, then also
        # specify your in-app conversion types so the Google Ads API can focus
        # your campaign on people who are most likely to complete the
        # corresponding in-app actions.
        #
        # campaign.selective_optimization.conversion_actions.extend(
        #     ["INSERT_CONVERSION_ACTION_RESOURCE_NAME_HERE"]
        # )

        # Submits the campaign operation and print the results.
        campaign_response = campaign_service.mutate_campaigns(
            customer_id=customer_id, operations=[campaign_operation]
        )
        resource_name = campaign_response.results[0].resource_name
        print(f'Created App campaign with resource name: "{resource_name}".')
        return resource_name

    def set_campaign_targeting_criteria(
        self, client, customer_id, campaign_resource_name
    ):
        """Sets campaign targeting criteria for a given campaign.

        Both location and language targeting are illustrated.

        Args:
            client: an initialized GoogleAdsClient instance.
            customer_id: a client customer ID str.
            campaign_resource_name: the campaign to apply targeting to
        """
        campaign_criterion_service = client.get_service("CampaignCriterionService")
        geo_target_constant_service = client.get_service("GeoTargetConstantService")
        googleads_service = client.get_service("GoogleAdsService")

        campaign_criterion_operations = []
        # Creates the location campaign criteria.
        # Besides using location_id, you can also search by location names from
        # GeoTargetConstantService.suggest_geo_target_constants() and directly
        # apply GeoTargetConstant.resource_name here. An example can be found
        # in targeting/get_geo_target_constant_by_names.py.
        for location_id in ["21137", "2484"]:  # California  # Mexico
            campaign_criterion_operation = client.get_type("CampaignCriterionOperation")
            campaign_criterion = campaign_criterion_operation.create
            campaign_criterion.campaign = campaign_resource_name
            campaign_criterion.location.geo_target_constant = (
                geo_target_constant_service.geo_target_constant_path(location_id)
            )
            campaign_criterion_operations.append(campaign_criterion_operation)

        # Creates the language campaign criteria.
        for language_id in ["1000", "1003"]:  # English  # Spanish
            campaign_criterion_operation = client.get_type("CampaignCriterionOperation")
            campaign_criterion = campaign_criterion_operation.create
            campaign_criterion.campaign = campaign_resource_name
            campaign_criterion.language.language_constant = (
                googleads_service.language_constant_path(language_id)
            )
            campaign_criterion_operations.append(campaign_criterion_operation)

        # Submits the criteria operations.
        for row in campaign_criterion_service.mutate_campaign_criteria(
            customer_id=customer_id, operations=campaign_criterion_operations
        ).results:
            print(
                "Created Campaign Criteria with resource name: "
                f'"{row.resource_name}".'
            )

    def create_ad_text_asset(self, client, text):
        ad_text_asset = client.get_type("AdTextAsset")
        ad_text_asset.text = text
        return ad_text_asset

    def create_app_ad(self, client, customer_id, ad_group_resource_name, images):
        """Creates an App ad for a given ad group.

        Args:
            client: an initialized GoogleAdsClient instance.
            customer_id: a client customer ID str.
            ad_group_resource_name: the ad group where the ad will be added.
        """
        # Creates the ad group ad.
        ad_group_ad_service = client.get_service("AdGroupAdService")
        ad_group_ad_operation = client.get_type("AdGroupAdOperation")
        ad_group_ad = ad_group_ad_operation.create
        ad_group_ad.status = client.enums.AdGroupAdStatusEnum.ENABLED
        ad_group_ad.ad_group = ad_group_resource_name
        # ad_data is a 'oneof' message so setting app_ad
        # is mutually exclusive with ad data fields such as
        # text_ad, gmail_ad, etc.
        ad_group_ad.ad.app_ad.headlines.extend(
            [
                self.create_ad_text_asset(client, "A cool puzzle game"),
                self.create_ad_text_asset(client, "Remove connected blocks"),
            ]
        )
        ad_group_ad.ad.app_ad.descriptions.extend(
            [
                self.create_ad_text_asset(client, "3 difficulty levels"),
                self.create_ad_text_asset(client, "4 colorful fun skins"),
            ]
        )

        ad_group_ad.ad.app_ad.images.extend(images)

        ad_group_ad_response = ad_group_ad_service.mutate_ad_group_ads(
            customer_id=customer_id, operations=[ad_group_ad_operation]
        )
        ad_group_ad_resource_name = ad_group_ad_response.results[0].resource_name
        print(
            "Ad Group App Ad created with resource name:"
            f'"{ad_group_ad_resource_name}".'
        )

    def create_app_ad_group(self, client, customer_id, campaign_resource_name, payload):
        """Creates an ad group for a given campaign.

        Args:
            client: an initialized GoogleAdsClient instance.
            customer_id: a client customer ID str.
            campaign_resource_name: the campaign to be modified

        Returns:
            A resource_name str for the newly created ad group.
        """
        ad_group_service = client.get_service("AdGroupService")

        # Creates the ad group.
        # Note that the ad group type must not be set.
        # Since the advertising_channel_sub_type is APP_CAMPAIGN,
        #   1- you cannot override bid settings at the ad group level.
        #   2- you cannot add ad group criteria.
        ad_group_operation = client.get_type("AdGroupOperation")
        ad_group = ad_group_operation.create
        ad_group.name = payload.get("adGroup", f"Ad group {uuid.uuid4()}")
        ad_group.status = client.enums.AdGroupStatusEnum.ENABLED
        ad_group.campaign = campaign_resource_name

        ad_group_response = ad_group_service.mutate_ad_groups(
            customer_id=customer_id, operations=[ad_group_operation]
        )

        ad_group_resource_name = ad_group_response.results[0].resource_name
        print(f'Ad Group created with resource name: "{ad_group_resource_name}".')
        return ad_group_resource_name

    def create_app_ad_campaign(self, payload):
        client = self.client
        customer_id = self.customer_id

        image_assets = [{"asset": "customers/1252515370/assets/83805628661"}]
        # Creates the budget for the campaign.
        budget_resource_name = self.create_budget(client, customer_id)
        # Creates the campaign.
        campaign_resource_name = self.create_app_ad_campaign_resource(
            client, customer_id, budget_resource_name, payload
        )
        # # Sets campaign targeting.
        self.set_campaign_targeting_criteria(
            client, customer_id, campaign_resource_name
        )
        # Creates an Ad Group.
        ad_group_resource_name = self.create_app_ad_group(
            client, customer_id, campaign_resource_name, payload
        )
        # ad_group_resource_name = "customers/1252515370/adGroups/149625895175"
        # Creates an App Ad.
        if "images" in payload:
            image_assets = []
            for image in payload["images"]:
                image_assets.append(
                    self.upload_image_asset(image["url"], image["name"])
                )
        self.create_app_ad(client, customer_id, ad_group_resource_name, image_assets)


def handle_googleads_exception(exception):
    print(
        f'Request with ID "{exception.request_id}" failed with status '
        f'"{exception.error.code().name}" and includes the following errors:'
    )
    for error in exception.failure.errors:
        print(f'\tError with message "{error.message}".')
        if error.location:
            for field_path_element in error.location.field_path_elements:
                print(f"\t\tOn field: {field_path_element.field_name}")
    sys.exit(1)
